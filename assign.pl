#!/software/bin/perl -w
#
# Script to assign unique IDs to Darwin Tree of Life samples
# kj2 20191017
#
# perl assign.pl -excel <spreadsheet.xlsx>
#
# This takes a spreadsheet with correct column heades expecting 
# - common name
# - taxon id
# - donor id
# and produces
# - a list of values to be added to that spreadsheet (results.txt, just copy over)
# - another list that needs to be added to existing /nfs/team135/kj2/darwin_list/unique_ids_assigned.txt for next time (add2list.txt) 


use strict;
use Getopt::Long;
use Spreadsheet::ParseXLSX;

my $reference = "/nfs/team135/kj2/darwin-tree-of-life-sample-naming/master_species_list.txt"; # file containing all ID assignments
my $list      = "/nfs/team135/kj2/darwin-tree-of-life-sample-naming/unique_ids_assigned.txt"; # file containing all those IDs already assigned, needed for assigning the correct number
my $excel;    # excel spreadsheet with samples to be assigned a unique ID
my $common;
my $offset;
my $help;
my $stop;

GetOptions (
    'ref:s'   => \$reference,
    'list:s'  => \$list,
    'excel:s' => \$excel,
    'common'  => \$common, # use this switch for Sanger work request manifests where the "common name" column carries the species name
    'offset:s' => \$offset, # if there are any lines to be disregarded before the header
    'stop'    => \$stop, # if you want to drop out after error
    'h'       => \$help,
);

if ((!$excel) || ($help)) {
    print "usage: perl assign.pl -excel <filename>\n";
    print "                      -common           # if species name contained in \"common name\" column rather than \"species\" column\n";
    print "                      -offset <n>       # if the spreadsheet header is not in the first line\n";
    exit(0);
}

# know what unique ID is assigned to what species, remember taxid for sanity check
my (%reftax,%refspec);
open(IN,$reference);
while (<IN>) {
    next unless /^\S+/;
    my @a = split /\t/;
    $refspec{$a[1]}->{unique} = $a[0];
    $refspec{$a[1]}->{taxid}  = $a[2];
    $reftax{$a[2]}->{unique}  = $a[0];
    $reftax{$a[2]}->{spec}    = $a[1];
}

# check what has already been assigned
my %number;
my %donor;
open(AN,$list);
while (<AN>) {
    next unless (/\S+/);
    my ($unique,$species,$donorid,$number) = split /\t/;
    
    # know what the highest number is for the unique ID
    $number{$species} = $number unless ($number{$species} && ($number{$species} > $number));
    
    # know what number was assigned to what donor ID 
    $donor{$species}->{$donorid} = $number;
}



# now to the spreadsheet
my $parser   = Spreadsheet::ParseXLSX->new;
my $workbook = $parser->parse($excel); 


if ( !defined $workbook ) {
    die $parser->error(), ".\n";
}
 

#######################
# HERE GO THE RESULTS #
#######################
 
open(OUT,">./add2list.txt"); # the content of this list needs to be appended to /nfs/team135/kj2/darwin_list/unique_ids_assigned.txt for next time!!!
open(RES,">./result.txt"); # this needs to be copied into the spreadsheet


# and this is how we get to them
for my $worksheet ( $workbook->worksheets() ) {
 
    my ($speccol, $comcol, $taxidcol, $donidcol);
    
    my ( $row_min, $row_max ) = $worksheet->row_range();
    my ( $col_min, $col_max ) = $worksheet->col_range();

    # identify location of relevant columns 
    foreach my $col ($col_min..$col_max) {
        next unless ($worksheet->get_cell(0,$col));
        #print $col, "\t",$worksheet->get_cell(0,$col)->value,"\n";
        my $colstart = 0;
        $colstart += $offset if ($offset);
        my $cell = $worksheet->get_cell($colstart,$col)->value; # might have to remove
        #my $cell = $worksheet->get_cell(0,$col)->value unless ($common); # might have to uncomment
        #$cell = $worksheet->get_cell(8,$col)->value if ($common); # might have to uncomment
        $speccol  = $col if (($cell =~ /^species$/i) || ($cell =~ /^scientific_name$/i));
        $speccol  = $col if (($cell =~ /^common name/i) && ($common)); # if using Sanger work request manifest, see above
        $taxidcol = $col if (($cell =~ /^taxon id/i) || ($cell =~ /^taxon_id/i));
        $donidcol = $col if (($cell =~ /^specimen_id$/i) || ($cell =~ /^donor id/i) || ($cell =~ /^donor_id/i));
    }
    my $start = 1;
    $start += $offset if ($offset);
    
    # data
    foreach my $row ($start..$row_max) {
        next unless ($worksheet->get_cell($row,$speccol));
        my $species = $worksheet->get_cell($row,$speccol)->value;
        next unless ($species =~ /\S+/);
        $species =~ s/\s+/ /g;
        $species =~ s/^\s//g;
        $species =~ s/\s$//g;
        #($species) = ($species =~ /^(\S+\s+\S+)/);
        #my $common  = $worksheet->get_cell($row,$comcol)->value;
        my $taxid   = $worksheet->get_cell($row,$taxidcol)->value;
        $taxid =~ s/^\s//g;
        $taxid =~ s/\s$//g;
        my $donorid = $worksheet->get_cell($row,$donidcol)->value; 
        $donorid =~ s/^\s//g;
        $donorid =~ s/\s$//g;


        my $nospec;
        # is the species identifed?
        if ($species =~ /sp.$/) {
            print "ERROR: Genus only for $species, not assigning uniqueID\n";
            $nospec++;
        }
        
        # does the species name exist? if not this needs to be fixed in the reference list
        unless (exists $refspec{$species}->{unique}) {
            print "ERROR: Cannot find species $species in the master list" unless ($nospec);
            
            # does a different species name exist for the same taxID?
            if (exists $reftax{$taxid}->{unique}) {
                print " maybe take species name based on taxID: ", $reftax{$taxid}->{species}," and assign ",$reftax{$taxid}->{unique},"?" unless ($nospec);
            }
            print "\n" unless ($nospec);
            exit(0) if ($stop);
        }
        
        # does a taxid exist and match? If not, check why not and if necessary amend refence list or spreadsheet
        if (exists $refspec{$species}->{taxid}) {
            unless ($refspec{$species}->{taxid} =~ /^$taxid$/i) {
                print "ERROR: $species taxId in sheet ($taxid) does not match expected value (",$refspec{$species}->{taxid},")\n";
            }
        }
        
        my $uniqueid = $refspec{$species}->{unique};
        my $uniquenum;
                
        # do we already have something assigned to this?
        if (exists $number{$species}) {
            
            # does a number exist for the same donor already? take it
            if (exists $donor{$species}->{$donorid}) {
                $uniquenum = $donor{$species}->{$donorid};  
            }
            
            # if not make one
            else {
                $number{$species}++;
                $uniquenum = $number{$species};
                $donor{$species}->{$donorid} = $uniquenum;
                print OUT join "\t", ($uniqueid.$uniquenum,$species,$donorid,$uniquenum), "\n";
            }
            print RES  $uniqueid.$uniquenum, "\n"; 
        }
        # nope, make one
        else {
            if ($nospec) {
                print RES "\n";
            }
            else {
                $number{$species}++;
                $uniquenum = $number{$species};
                $donor{$species}->{$donorid} = $uniquenum;
                print OUT join "\t", ($uniqueid.$uniquenum,$species,$donorid,$uniquenum), "\n";
                print RES  $uniqueid.$uniquenum, "\n"; 
            }        
        }
        
    
    }
} 

