This project contains the master files for DToL (https://www.darwintreeoflife.org/) sample naming. 

They consist of
- a master list that contains the assignments of IDs to species. This is not assumed to be comprehensive.
- a unique ID list that contains the actual assignments of real unique IDs to individual samples.
- a list of prefix assignments for clades.

------

A public name is a unique identifier for an individual sampled for DToL and consists of 
- a lower case letter for the high level clade (see prefix assignment)
- a lower case letter for the clade (not for vertebrates, VGP legacy)
- one upper, two lower case letters for genus
- one upper, three lower case letters for species (one upper, two lower case for vertebrates, VGP legacy)
- a number to indicate the individual that was sampled
=> e.g. aRanTem1 for the first sampled individual of Rana temporaria, xgPerPere3 for the third sampled individual of Peregriana peregra

The master_species_list.txt contains assignments of public name prefixes (i.e. the above without the number) for all expected species in the UK. It is not complete and not correct everywhere, but a starting point to pre-empt duplication issues. Errors are corrected were identified and new species added where necessary. 

Examples:
<Prefix, species, taxID, common name, genus, family, order, class, phylum (the classification is and stays controversial)>
- xgDanTine  Danilia tinei None	None Danilia	Chilodontidae  Seguenziida Gastropoda	Mollusca
- fSalSal Salmo salar	8030 Atlantic salmon	Salmo Salmonidae	Salmoniformes  Actinopterygii Chordata


The unique_ids_assigned.txt file keeps a record of what was actually assigned (i.e. the number) and links a specific public name with the collector’s donor ID for the sample. Both lists are queried during a public name assignment to also catch resubmissions of the same sample. 

Examples:
<public name, species, donor ID, number>
- ilTyrJaco1      Tyria jacobaeae Ox0023  1       
- ilAlcRepa1      Alcis repandata Ox0024  1       
- ilHemAest1      Hemithea aestivaria     Ox0025  1       

On receipt of samples and a sample manifest we check whether the taxID and species name exist and match each other and the master list.
- If there is no taxID yet, a new one will be requested from taxonomyDB
- if taxID and species name don’t match (or if they don’t match taxonomyDB or the master list entry) we enquire with the collectors. Any necessary corrections are applied.
- if there is no master list entry, it is added according to the rules above, making sure the prefix is unique.

The results of the assignment are added to the sample manifest and the unique_ids_assigned.txt list.

For naming genome assemblies of samples, we recommend to use the above and add .<version>

Examples:
fCotGob3.1 (first assembly version of the 3rd individual of Cottoperca gobio)
fAstCal1.2 (second assembly version of the first individual of Astatotilapia calliptera) 

-----

Kerstin Howe, Tree of Life Programme, Wellcome Sanger Institute, UK, 16.06.2020
